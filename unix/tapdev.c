/*
 * Copyright (c) 2001, Swedish Institute of Computer Science.
 * All rights reserved. 
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met: 
 *
 * 1. Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 *
 * 3. Neither the name of the Institute nor the names of its contributors 
 *    may be used to endorse or promote products derived from this software 
 *    without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE. 
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 * $Id: tapdev.c,v 1.7.2.1 2003/10/07 13:23:19 adam Exp $
 */


#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#ifdef linux
#include <sys/ioctl.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#define DEVTAP "/dev/net/tun"
#else  /* linux */
#define DEVTAP "/dev/tap0"
#endif /* linux */

#include "uip.h"
#define ETHER_HDR_SIZE 14

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

/* The TCP and IP headers. */
struct ip_tcp_hdr  {
	u8		ip_hl_v;	/* header length and version	*/
	u8		ip_tos;		/* type of service		*/
	u16		ip_len;		/* total length			*/
	u16		ip_id;		/* identification		*/
	u16		ip_off;		/* fragment offset field	*/
	u8		ip_ttl;		/* time to live			*/
	u8		ip_p;		/* protocol			*/
	u16		ip_sum;		/* checksum			*/
	struct in_addr	ip_src;		/* Source IP address		*/
	struct in_addr	ip_dst;		/* Destination IP address	*/
	/* TCP header. */
	u16		tcp_src;	/* tcp source port		*/
	u16		tcp_dst;	/* tcp destination port		*/
	u32     tcp_seqno;
	u32     tcp_ackno;
	u8      tcp_offset;
	u8      tcp_flags;
	u16		udp_wnd;
	u16		udp_chksum;
	u16		udp_urgp;
};

static int fd;

static unsigned long lasttime;
static struct timezone tz;

/*-----------------------------------------------------------------------------------*/
void ip_to_string(struct in_addr x, char *s)
{
	x.s_addr = ntohl(x.s_addr);
	sprintf(s, "%d.%d.%d.%d",
		(int) ((x.s_addr >> 24) & 0xff),
		(int) ((x.s_addr >> 16) & 0xff),
		(int) ((x.s_addr >> 8) & 0xff),
		(int) ((x.s_addr >> 0) & 0xff)
	);
}

void print_tcpiphead(struct ip_tcp_hdr* tcpip)
{

	printf("version is %u \n", (tcpip->ip_hl_v >> 4));
	printf("header length is %u \n", (tcpip->ip_hl_v & 0x00ff));
	printf("type of service is %u \n", (tcpip->ip_tos));
	printf("total length is %hu \n", ntohs(tcpip->ip_len));
	printf("identification is %hu \n", ntohs(tcpip->ip_id));
	printf("fragment offset field is %hu \n", ntohs(tcpip->ip_off));
	printf("time to live is %u \n", (tcpip->ip_ttl));
	printf("protocol is %u \n", (tcpip->ip_p));
	printf("checksum is %hu \n", ntohs(tcpip->ip_sum));
	char ipaddr[17];
	ipaddr[16] = '\0'; 
	ip_to_string(tcpip->ip_src, &ipaddr[0]);
	printf("ip_src is %s \n", &ipaddr[0]);
	ip_to_string(tcpip->ip_dst, &ipaddr[0]);
	printf("ip_dst is %s \n", &ipaddr[0]);
	printf("tcp_src port is %hu \n", ntohs(tcpip->tcp_src));
	printf("tcp_dst port is %hu \n", ntohs(tcpip->tcp_dst));
	printf("tcp_seqno is %u \n", ntohl(tcpip->tcp_seqno));
	printf("tcp_ackno is %u \n", ntohl(tcpip->tcp_ackno));
	printf("tcp_offset is %u \n", (tcpip->tcp_offset));
	printf("tcp_flags is %u \n", (tcpip->tcp_flags));
	printf("tcp_wnd is %hu \n", ntohs(tcpip->udp_wnd));
	printf("tcp_chksum is %hu \n", ntohs(tcpip->udp_chksum));
	printf("tcp_urgp is %hu \n", ntohs(tcpip->udp_urgp));   
}

void
tapdev_init(void)
{
  char buf[1024];
  
  fd = open(DEVTAP, O_RDWR);
  if(fd == -1) {
    perror("tapdev: tapdev_init: open");
    exit(1);
  }

#ifdef linux
  {
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));
    ifr.ifr_flags = IFF_TAP|IFF_NO_PI;
    if (ioctl(fd, TUNSETIFF, (void *) &ifr) < 0) {
      perror(buf);
      exit(1);
    }
  }
#endif /* Linux */

  snprintf(buf, sizeof(buf), "ifconfig tap0 inet %d.%d.%d.%d",
	   UIP_DRIPADDR0, UIP_DRIPADDR1, UIP_DRIPADDR2, UIP_DRIPADDR3);
  system(buf);

  lasttime = 0;
}
/*-----------------------------------------------------------------------------------*/
unsigned int
tapdev_read(void)
{
  struct ip_tcp_hdr *tcpip;
  fd_set fdset;
  struct timeval tv, now;
  int ret;
  
  if(lasttime >= 500000) {
    lasttime = 0;
    return 0;
  }
  
  tv.tv_sec = 0;
  tv.tv_usec = 500000 - lasttime;


  FD_ZERO(&fdset);
  FD_SET(fd, &fdset);

  gettimeofday(&now, &tz);  
  ret = select(fd + 1, &fdset, NULL, NULL, &tv);
  if(ret == 0) {
    lasttime = 0;    
    return 0;
  } 
  ret = read(fd, uip_buf, UIP_BUFSIZE);  
  if(ret == -1) {
    perror("tap_dev: tapdev_read: read");
  }
  printf("c------------>s\n");
  tcpip = (struct ip_tcp_hdr *)(uip_buf + ETHER_HDR_SIZE);
  print_tcpiphead(tcpip);
  gettimeofday(&tv, &tz);
  lasttime += (tv.tv_sec - now.tv_sec) * 1000000 + (tv.tv_usec - now.tv_usec);

  return ret;
}
/*-----------------------------------------------------------------------------------*/
void
tapdev_send(void)
{
  int ret;
  struct iovec iov[2];
  struct ip_tcp_hdr *tcpip;
  
#ifdef linux
  {
    char tmpbuf[UIP_BUFSIZE];
    int i;

    for(i = 0; i < 40 + UIP_LLH_LEN; i++) {
      tmpbuf[i] = uip_buf[i];
    }
    
    for(; i < uip_len; i++) {
      tmpbuf[i] = uip_appdata[i - 40 - UIP_LLH_LEN];
    }
    printf("s------------>c\n");
    tcpip = (struct ip_tcp_hdr *)(tmpbuf + ETHER_HDR_SIZE);
	print_tcpiphead(tcpip);
    ret = write(fd, tmpbuf, uip_len);
  }  
#else 

  if(uip_len < 40 + UIP_LLH_LEN) {
    ret = write(fd, uip_buf, uip_len + UIP_LLH_LEN);
  } else {
    iov[0].iov_base = uip_buf;
    iov[0].iov_len = 40 + UIP_LLH_LEN;
    iov[1].iov_base = (char *)uip_appdata;
    iov[1].iov_len = uip_len - (40 + UIP_LLH_LEN);  
    
    ret = writev(fd, iov, 2);
  }
#endif
  if(ret == -1) {
    perror("tap_dev: tapdev_send: writev");
    exit(1);
  }
}  
/*-----------------------------------------------------------------------------------*/
